A python script that manages a DB of publications for a research group, updates them with info from ArXiv/Orcid/DOI, and then renders them in HTML to be embedded on a website.

## Installation

You can install this using `pipx`
```
pipx install git+https://anlucia@bitbucket.org/anlucia/group-publications.git
```

## Usage

By default, `groupub` uses a db located in a application config directory (depending on the OS you are using), but you can change the default location by passing `--db [FILE]` as a command line option (before specifying the rest of the commands).

There are two main commands groups: `author` and `pub`.

### `author`

The `author` command group is related to adding and modifying the list of authors.

```
Commands:
  add      Add a new author
  cleanup  Removes unused author fields
  edit     Edit author information
  list     List authors
  remove   Remove an author from the list (but not their publications)
```

Authors should have a name and an OrcID number, at least. It is possible to specify a arxiv query, e.g., `au:Surname_N` for an author named N. Surname (unfortunately the current version of the arxiv API does not distinguish between authors with different first names).

### `pub`

The `pub` command group is responsible for searching for new authors publications, updating the information about the known ones, making edits, and rendering them in HTML using a template.

```
Commands:
  drop     Remove all publications
  edit     Edit publication
  exclude  Exclude publication
  list     List all publications
  render   Renders HTML output of publication list (exluding excluded...
  search   Search publications for all authors
  update   Update all known pubs
```

Publications should at least be on the arxiv. In the rare cases when this does not happen, some commands will break, but it might be possible to still make some things works by adding the pubblication by hand in the db using `TinyDB` directly. Please put everything on the arxiv.

## Example of usage

The process of updating the list of publications is roughly a three step process.

1. `groupub search` to search for new publications. In case of doubt, it will ask whether to include them on the prompt.
1. `groupub update` will search for new information about the known publications (i.e., if some pre-print has been published)
1. `groupub render` will generate a HTML rendering of the known publications.

## A few details

- Publications can be excluded from the shown list. This is done automatically in some cases (e.g., authors name which do not match), but can be done manually with `groupub pub exclude`
- When editing publications, the changes are not overwritten on the original data, but saved on a "overlay" database. This is to avoid the changes to be reverted when the publications are updated. This feature should be only used to cover some special corner cases.

## Todo list
- I am still not quite happy about how the code is organised. Here are some random thoughts:
    + Maybe `sources.py` should be split into different files for each source, instead of having class methods everywhere
