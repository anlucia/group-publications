from dataclasses import dataclass, fields
import inspect

from datetime import date
from dateutil.parser import isoparse

def ts_to_year(s):
    dt = isoparse(s)
    return dt.strftime('%Y')

def date_parts_to_date(dp):
    """Parses date parts [ [year, month, day] ].

    Only year is required."""

    i = iter(dp[0])
    year = next(i)
    month = next(i, 1)
    day = next(i, 1)
    return date(year, month, day)

def extract_date(df):
    """Extract a ISO 8601 date from a DOI date field"""
    if 'date-time' in df:
        return df['date-time']
    elif 'date-parts' in df:
        return date_parts_to_date(df['date-parts']).isoformat()

def year_in_range(year, start, end=None):
    if end:
        return year in range(start, end)
    else:
        return year >= start

def year_in_ranges(year, year_ranges):
    # If year_ranges is empty, always accept
    # This allows us to have authors whose publications will always be included,
    # no matter the year
    if not year_ranges:
        return True

    return any(year_in_range(year, r['start'], r.get('end') ) for r in year_ranges)

@dataclass
class Publication:
    short_id: str = None
    id: str = None
    doi: str = None
    title: str = None
    authors: str = None
    pubdate: str = None
    updated: str = None
    pdf_link: str = None
    journal_ref: str = None

    @classmethod
    def keys(cls):
        return inspect.signature(cls).parameters

    @classmethod
    def from_dict(cls, env):
        """Build from dictionary, keeping only the keys we are interested in."""
        return cls(**{
            k: v for k, v in env.items()
            if k in cls.keys()
        })

    def update_from_dict(self, d):
        for key, value in d.items():
            if hasattr(self, key):
                setattr(self, key, value)

    def update(self, new, delete=False ):
        for key in self.keys():
            value = getattr(self, key)
            newvalue = getattr(new, key, None)
            if newvalue is not None or delete:
                setattr(self, key, newvalue)

    @staticmethod
    def shorten_id(long_id):
        """Returns just the numeric arxiv paper id, without the URL nor the versioning number"""
        short_id = long_id.split('arxiv.org/abs/')[-1].split('arXiv:')[-1].split('v')[0]
        return short_id

    def date(self):
        """Return the publication date.

        If the Publication has a DOI, return the DOI publication date.
        If this is missing or we do not have a DOI (i.e. this is a pre-print), return the most recent arXiv update date.
        """
        if self.doi and self.pubdate:
            return self.pubdate
        else:
            return self.updated

    def year(self):
        return ts_to_year(self.date())

    def is_in_ranges(self, ranges):
        return year_in_ranges(self.year(), ranges)
