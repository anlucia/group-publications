import click
import json
import re

from tinydb import TinyDB, Query
from tinydb.operations import delete

pass_db = click.make_pass_decorator(TinyDB)
Author = Query()


@click.group()
def author():
    """Manage authors"""
    pass

def get_author(db, name):
    authors = db.table('authors')
    result = authors.search(Author.name.matches(name, flags=re.IGNORECASE))
    if result:
        return result[0]
    else:
        return {}

def ask_author_info(new_author={}):
    return new_author

@author.command()
@click.argument('name', required=False)
@pass_db
def list(db, name=None):
    """List authors"""
    authors = db.table('authors')
    if not name:
        result = authors
    else:
        result = authors.search(Author.name.matches(name, flags=re.IGNORECASE))
    if result:
        for row in result:
            click.echo(row)
    else:
         click.echo("No authors found")

@author.command()
@click.option('--name', prompt="Enter author full name: ")
@pass_db
def add(db, name):
    """Add a new author"""
    authors = db.table('authors')
    name = name.strip()
    new_author = get_author(db, name)
    if new_author:
        click.echo(f"Author{name} already exists.")
        return

    new_author['name'] = name
    new_author['orcid'] = click.prompt("Enter author Orcid: ", default = new_author.get('orcid')).strip()
    new_author['query'] = click.prompt("Enter author query search string: ", default = new_author.get('query')).strip()

    authors.upsert(new_author, Author.name == name)
    click.echo(new_author)

@author.command()
@click.option('--name', prompt="Enter author full name")
@pass_db
def edit(db, name):
    """Edit author information"""
    authors = db.table('authors')
    name = name.strip()
    author_data  = get_author(db, name)
    if not author_data:
        click.echo(f"Author {name} not found")
        return
    author_data = json.loads(click.edit(json.dumps(author_data, indent=2, ensure_ascii=False), require_save=False))
    authors.update(author_data, Author.name == name)
    click.echo(author_data)

@author.command()
@click.option('--name', prompt="Enter author full name: ")
@pass_db
def remove(db, name):
    """Remove an author from the list (but not their publications)"""
    authors = db.table('authors')
    name = name.strip()
    result = authors.remove(Author.name == name)
    if result:
        click.echo(f"Removed author {name}")
    else:
        click.echo("Cannot remove: author {name} not found")

@author.command()
@click.confirmation_option(prompt='Are you sure you want to cleanup unused author fields?')
@pass_db
def cleanup(db):
    """Removes unused author fields"""
    authors = db.table('authors')
    click.echo("Removing au field")
    authors.update(delete('au'), Author.au.exists())
    click.echo("Removing idx field")
    authors.update(delete('idx'), Author.idx.exists())
