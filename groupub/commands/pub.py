import click
import json

from tinydb import TinyDB, Query
import re

from groupub.sources import *
from groupub.publication import Publication
from dataclasses import asdict

from datetime import datetime
from dateutil.parser import isoparse
from jinja2 import Template

pass_db = click.make_pass_decorator(TinyDB)
Pub = Query()

@click.group()
def pub():
    """Manage publications"""
    pass

def all_known_pubs(db, sort=False, include_excluded=False, with_edits=False):
    group_pubs = db.table('publications')
    edits = db.table('edits')

    if include_excluded:
        data = group_pubs.all()
    else:
        data = group_pubs.search(~(Pub.exclude == True))

    if with_edits:
        for pub in data:
            pub.update( edits.get(Pub.short_id == pub['short_id']) or {})

    pubs = [ Publication.from_dict(d) for d in  data ]

    if sort:
        pubs.sort(key=lambda p: p.date(), reverse=True)
    return pubs

def add_or_update_pub(db, p, ask_confirmation=True):
    # we are not currently adding publications which are not on the arXiv
    if p and not p.short_id:
        return

    group_pubs = db.table('publications')

    data = asdict(p)
    if not group_pubs.search(Pub.short_id == p.short_id):
        click.echo(f"\n New publication found: {p.short_id}: {p.id}")
        click.echo(f"{p.authors}. {p.title}")
        if ask_confirmation and input(f"Do you want to add this publication to the list? (yes/no):") == "no":
            data['exclude'] = True

    group_pubs.upsert(data, Pub.short_id == p.short_id)

def update_pub(db, p, extra_query=None):
    """Update a single publication.

    If we have a DOI number, we will get first the info from there, then we will serch the arXiv."""
   # click.echo(f"Updating {p.short_id}")
    if p.doi:
        add_or_update_pub(db, Doi.update_pub(p), ask_confirmation=False)

    res = Arxiv.update_pub(p, extra_query=extra_query) or []
    if len(res) == 1 and res[0]:
        add_or_update_pub(db, res[0], ask_confirmation=False)
    else:
        for r in res:
            newpub = p.update(r)
            add_or_update_pub(db, newpub, ask_confirmation=True)


@pub.command()
@pass_db
@click.confirmation_option(prompt='Are you sure you want to drop the table of publications?')
def drop():
    """Remove all publications"""
    click.echo("Dropping table Publications")
    db.drop_table('publications')

@pub.command()
@click.option('--all', is_flag=True, default=False, help="Show excluded publications")
@click.option('--with-edits/--no-edits', default=True, help="Apply local edits (defaults to true)")
@pass_db
def list(db, all, with_edits):
    """List all publications"""
    click.echo_via_pager( f"[{1+idx}] {p.short_id} {p.authors}. {p.title} ({p.pdf_link}) \n"
                          for idx, p in enumerate(all_known_pubs(db, sort=True, include_excluded=all, with_edits=with_edits)))

@pub.command()
@click.option("--template", required=True, help="Jinja2 template file", type=click.File(mode='r'))
@click.option("--output", default="publications.html", help="Output file", type=click.File(mode='w'))
@pass_db
def render(db, template, output):
    """Renders HTML output of publication list (exluding excluded publications and applying local edits)."""
    j2tp = Template(template.read())
    output.write(j2tp.render(pubs = all_known_pubs(db, sort=True, with_edits=True)))


@pub.command()
@pass_db
def update(db):
    """Update all known pubs"""
    with click.progressbar(all_known_pubs(db),
                           item_show_func = lambda p: f"Updating {p.short_id}" if p else None
    ) as bar:
        for p in bar:
            update_pub(db, p)

@pub.command()
#@click.option('--author', required=False)
@pass_db
def search(db):
    """Search publications for all authors"""
    authors = db.table('authors')
    group_pubs = db.table('publications')

    # Arxiv Author ID
    with click.progressbar( [ author for author in authors if author.get('orcid') or author.get('arxivid')],
                            item_show_func = lambda author: f"Getting data for {author['name']} from {Arxiv.AUTHOR_URL.format(author= author.get('orcid') or author.get('arxivid'))}" if author else None,
                            show_eta = False, show_percent=False, show_pos=True
    ) as bar:
        for author in bar:
            idx = author.get('orcid') or author.get('arxivid')
            for p in Arxiv.search_author(idx):
                add_or_update_pub(db, p, ask_confirmation=False)

    # Orcid
    with click.progressbar( [ author for author in authors if author.get('orcid') ],
                            item_show_func = lambda author: f"Getting data for {author['name']} from {Orcid.AUTHOR_URL.format(author=author['orcid'])}" if author else None,
                            show_eta = False, show_percent=False, show_pos=True
    ) as bar:
        for author in bar:
            for p in Orcid.search_author(author['orcid']):
                # If we have not already seen this publications, we try to fill in some information
                if (p.doi and not group_pubs.search(Pub.doi.matches(p.doi, flags=re.IGNORECASE))) or (p.short_id and not group_pubs.search(Pub.short_id == p.short_id)):
                    update_pub(db, p, extra_query = author['query'])

    # Arxiv Query
    with click.progressbar( [ author for author in authors if author.get('query') ],
                            item_show_func = lambda author: f"Search query: {author['query']}"  if author else None,
                            show_eta = False, show_percent=False, show_pos=True
    ) as bar:
        for author in bar:
            for p in Arxiv.search_query(author['query']):
                add_or_update_pub(db, p, ask_confirmation=author['name'] not in p.authors)


@pub.command()
@click.option('--short-id', prompt="Enter short-id")
@pass_db
def exclude(db, short_id):
    """Exclude publication"""
    group_pubs = db.table('publications')

    short_id = short_id.strip()
    pub = group_pubs.get(Pub.short_id == short_id)
    if not pub:
        click.echo("Publication not found!")
        return

    pub['exclude'] = True
    group_pubs.upsert(pub, Pub.short_id == short_id)
    click.echo(f"Publication {short_id} excluded")
    return

@pub.command()
@click.option('--short-id', prompt="Enter short-id")
@pass_db
def edit(short_id):
    """Edit publication"""
    group_pubs = db.table('publications')
    edits = db.table('edits')

    short_id = short_id.strip()
    original = group_pubs.get(Pub.short_id == short_id)
    if not original:
        click.echo("Publication not found!")
        return

    # pub is a copy of original with edits applied
    pub = {**original, **(edits.get(Pub.short_id == short_id) or {}) }
    edited = json.loads(click.edit(json.dumps(pub, indent=2, ensure_ascii=False), require_save=False))
    diff = { k:edited[k] for k in original.keys() if original[k] != edited[k]  }
    click.echo(f"Edits: {diff}")
    if diff:
        # aways include 'short_id'
        diff['short_id'] = pub['short_id']
        edits.upsert(diff, Pub.short_id == short_id)
    else:
        edits.remove(Pub.short_id == short_id)
