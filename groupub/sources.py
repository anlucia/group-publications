import feedparser
import requests
from urllib.parse import quote
from ratelimit import limits, sleep_and_retry
from simplejson.errors import JSONDecodeError

from groupub.publication import *

class Arxiv:
    AUTHOR_URL = "https://arxiv.org/a/{author}.json"
    QUERY_URL = "https://export.arxiv.org/api/query?search_query={query}&start=0&max_results=300&sortBy=lastUpdatedDate&sortOrder=descending"
    ID_URL = "https://export.arxiv.org/api/query?id_list={ids}&start=0&max_results=1"

    @staticmethod
    def pub_from_arxiv_api(data):
        """Process dict coming from data API"""
        d = data
        for link in data['links']:
            if link.get('title') == 'pdf':
                d['pdf_link'] = link['href']
        d['journal_ref'] = data.get('arxiv_journal_ref')
        d['doi'] = data.get('arxiv_doi')
        d['authors'] = ", ".join([ author['name'] for author in data['authors']])
        d['short_id'] = Publication.shorten_id(data['id'])

        return Publication.from_dict(d)

    @staticmethod
    def pub_from_arxiv_author(data):
        """Process dict coming from data author page (via Orcid or data authorid)"""
        d = data
        d['pdf_link'] = data['formats']['pdf']
        d['short_id'] = Publication.shorten_id(data['id'])
        return Publication.from_dict(d)

    @classmethod
    @sleep_and_retry
    @limits(calls=4, period=1)
    def search_arxiv_api(cls, url):
        results = feedparser.parse(url)
        if results.status == 200:
            return [ cls.pub_from_arxiv_api(p) for p in results['entries'] ]
        else:
            return []
    @classmethod
    @sleep_and_retry
    @limits(calls=1, period=1)
    def search_author(cls, author):
        req = requests.get(cls.AUTHOR_URL.format(author=author))
        if req.ok:
            return [ cls.pub_from_arxiv_author(p) for p in  (req.json())['entries'] ]
        else:
            return []

    @classmethod
    def search_query(cls, query):
        return cls.search_arxiv_api(cls.QUERY_URL.format(query=quote(query)))

    @classmethod
    def search_short_id(cls, short_id):
        """Searches for a arxiv paper by short id"""
        return cls.search_arxiv_api(cls.ID_URL.format(ids=quote(short_id)))

    @classmethod
    def search_doi(cls, doi, extra_query=None):
        """Searches for a doi. It is useful to specify an author (by setting extra_query to the appropriate author query), as the arxiv API is a bit fuzzy otherwise"""
        query = f"doi:{doi}"
        if extra_query:
            query = f"{extra_query}+AND+" + query
        return cls.search_query(query)

    @classmethod
    def update_pub(cls, pub, extra_query = None):
        """Searches the arxiv for the publication, and updates it with the information obtained there.
        It is possible to specify extra query terms (such as an author search query), in order to refine the search.

        If more than one result is return from the arXiv API, it won't update pub but return the full list of publications."""
        res = []
        if pub.short_id:
            # We have a short_id, we can get an exact match in the arxiv API
            res =  cls.search_short_id(pub.short_id)
        elif not pub.short_id and pub.doi:
            # We do not have a short_id, but we have a doi
            # Let us search the arxiv for that doi and see if we get a match
            res = cls.search_doi(pub.doi, extra_query)

        if len(res) == 1:
            # we have found exactly one result, so we can use it to update our info
            pub.update(res[0])
            return [pub]
        elif len(res) > 1:
            print(res)
            return res


class Orcid:
    AUTHOR_URL = "https://orcid.org/{author}"

    @classmethod
    def get_author_data(cls, author):
        req = requests.get(cls.AUTHOR_URL.format(author=author), headers={'Accept': 'application/json'})
        if req.ok:
            return req.json()

    @staticmethod
    def pub_from_orcid_work(works):
        external_ids = works['external-ids']['external-id']
        doi =  next( (i['external-id-value'] for i in external_ids if i['external-id-type'] == 'doi') , None)
        arxivid = next( (i['external-id-value'] for i in external_ids if i['external-id-type'] == 'arxiv') , None)
        short_id = None
        if arxivid:
            short_id = Publication.shorten_id(arxivid)
        if short_id or doi:
            return Publication(short_id = short_id, doi = doi)
        else:
            return None

    @classmethod
    @sleep_and_retry
    @limits(calls=1, period=1)
    def search_author(cls, author):
        data = cls.get_author_data(author)
        if data and 'activities-summary' in data and 'works' in data['activities-summary']:
            pubs =  [ cls.pub_from_orcid_work(p) for p in data['activities-summary']['works'].get('group', []) ]
            return [ p for p in pubs if p ]

class Doi:
    ID_URL = 'https://dx.doi.org/{doi}'

    @staticmethod
    def pub_from_doi(data):
        d = dict()
        d['doi'] = data.get('DOI')
        d['title'] = data.get('title')
        d['authors'] = ", ".join( f"{author.get('given')} {author.get('family')}" for author in data.get('author',[]))
        d['pubdate'] = extract_date( data.get('issued'))
        # d['pdf_link'] = data['URL'] # let not take the url from the doi, as we prefer the arxiv pdf link
        d['journal_ref'] = data.get('container-title-short') or data.get('container-title')
        ref = [ data.get('volume'), data.get('issue'), data.get('article-number')]
        d['journal_ref'] += " " + ", ".join( r for r in ref if r)
        d['journal_ref'] += f" ({ts_to_year(d['pubdate'])})"

        return Publication.from_dict(d)

    @classmethod
    @sleep_and_retry
    @limits(calls=1, period=1)
    def search_doi(cls, doi):
        req = requests.get(cls.ID_URL.format(doi=doi), headers={'Accept': 'application/json'})
        if req.ok:
            try:
                return cls.pub_from_doi(req.json())
            except JSONDecodeError:
                return None

    @classmethod
    def update_pub(cls,pub):
        if not pub.doi:
            return pub

        pub.update(cls.search_doi(pub.doi))
        return pub
