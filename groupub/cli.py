import click
from groupub.commands.author import author
from groupub.commands.pub import pub

from tinydb import TinyDB, Query

from platformdirs import user_data_dir
import os.path
DEFAULT_DB_FILE = os.path.join( user_data_dir("Groupub", "Groupub"), 'db.json' )

@click.group()
@click.option('--db', type=click.Path(), help='Location of db', default=DEFAULT_DB_FILE)
@click.pass_context
def cli(ctx, db):
    """A command line tool to manage a list of group publications."""
    if not os.path.exists(os.path.dirname(db)):
        raise click.UsageError(f"Unable to open {db} since {os.path.dirname(db)} does not exists.")
    ctx.obj = TinyDB(db)

cli.add_command(author)
cli.add_command(pub)

if __name__ == '__main__':
    cli()
